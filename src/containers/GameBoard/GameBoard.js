import { connect } from 'react-redux';
import { moveItem, startOver } from '../../actions/index';
import GameBoard from '../../components/GameBoard/GameBoard';

const mapStateToProps = state => ({
    board: state.board,
    allowedToMoveItemsNumbers: state.allowedToMoveItemsNumbers,
    step: state.step,
    isComplete: state.isComplete
})

const mapDispatchToProps = {
    moveItem,
    startOver
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GameBoard)