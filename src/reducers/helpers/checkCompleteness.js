const checkCompleteness = (board) => {
    let orderedCubes = [].concat(...board);
    if (orderedCubes.pop().type !== 'empty') return false;
    
    const isComplete = orderedCubes.every((cube, index) => 
       !orderedCubes[index + 1] || (orderedCubes[index + 1] && orderedCubes[index + 1].number - cube.number === 1)
    )
    
    return isComplete;
};

export default checkCompleteness;