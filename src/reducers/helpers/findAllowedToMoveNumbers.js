const findAllowedToMoveNumbers = (board, {i, j}) => {
    let allowedNumbers = [];
    if (board[i][j - 1]) allowedNumbers.push(board[i][j - 1].number);
    if (board[i][j + 1]) allowedNumbers.push(board[i][j + 1].number);
    if (board[i - 1]) allowedNumbers.push(board[i - 1][j].number);
    if (board[i + 1]) allowedNumbers.push(board[i + 1][j].number);
    return allowedNumbers;
}

export default findAllowedToMoveNumbers;