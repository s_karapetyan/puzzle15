import { shuffle } from 'lodash-es';
import findAllowedToMoveNumbers from './findAllowedToMoveNumbers';
import checkCompleteness from './checkCompleteness';

const setInitialState = () => {
    const boardHeight = 4;
    const boardWidth = 4;
    let emptyItemIndexes = {};
    let cubes = createCubes(boardHeight, boardWidth);
    const board = createAndFillBoard(boardHeight, boardWidth, cubes, emptyItemIndexes);
    const allowedToMoveItemsNumbers = findAllowedToMoveNumbers(board, emptyItemIndexes);

    return({
        board,
        emptyItemIndexes,
        allowedToMoveItemsNumbers,
        step: 0,
        isComplete: checkCompleteness(board)
    })
}

const createCubes = (boardHeight, boardWidth) =>
    shuffle(
        Array.from(Array(boardHeight * boardWidth)).map((item, index) => {
            if (index !== boardHeight * boardWidth - 1) {
                return({
                    number: index + 1,
                    type: 'cube' 
                })
            } else {
                return({
                    type: 'empty'
                })
            }
        })
    )

const createAndFillBoard = (boardHeight, boardWidth, cubes, emptyItemIndexes) => 
    Array.from(Array(boardHeight)).map((row, i) =>
        Array.from(Array(boardWidth)).map((item, j) => {
            const cube = cubes.pop();
            if (cube.type === 'empty') {
                emptyItemIndexes.i = i;
                emptyItemIndexes.j = j;
            }
            return cube;
        })
    )


export default setInitialState;