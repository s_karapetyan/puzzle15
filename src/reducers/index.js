import setInitialState from './helpers/setInitialState';
import findAllowedToMoveNumbers from './helpers/findAllowedToMoveNumbers';
import checkCompleteness from './helpers/checkCompleteness';
const initialState = setInitialState();

const rootReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'MOVE_ITEM':
            const {i, j} = state.emptyItemIndexes;
            let newBoard = [...state.board];
            newBoard[i][j] = newBoard[action.i][action.j];
            const emptyItemIndexes = {
                i: action.i,
                j: action.j
            }
            newBoard[action.i][action.j] = {
                type: 'empty'
            };
            
            return({
                ...state,
                board: newBoard,
                emptyItemIndexes,
                allowedToMoveItemsNumbers: findAllowedToMoveNumbers(newBoard, emptyItemIndexes),
                step: state.step + 1,
                isComplete: checkCompleteness(newBoard)
            })

        case 'START_OVER':
            return({
                ...setInitialState()
            })

        default:
            return state;
    } 
}

export default rootReducer;