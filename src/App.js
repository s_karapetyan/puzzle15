import React, { Component } from 'react';
import './App.css';
import GameBoard from './containers/GameBoard/GameBoard';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="container">
          <GameBoard />
        </div>
      </div>
    );
  }
}

export default App;
