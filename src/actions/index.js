export const moveItem = (i, j) => 
    ({
        type: 'MOVE_ITEM',
        i,
        j
    })

export const startOver = () =>
    ({
        type: 'START_OVER'
    })