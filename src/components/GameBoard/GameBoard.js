import React from 'react';
import PropTypes from "prop-types";

const GameBoard = ({ board, allowedToMoveItemsNumbers, step, isComplete, moveItem, startOver }) => 
        <div className="game-container">
            <h2>Пазл Пятнашки</h2>
            <table className="table table-bordered game-container__board">
                <tbody>
                { board.map((row, i) => 
                    <tr key={i}>
                    { row.map((item, j) => 
                            <td key={j} { ...(allowedToMoveItemsNumbers.includes(item.number) && !isComplete) ? {className: "game-container__clickable-cube" } : {} } { ...(allowedToMoveItemsNumbers.includes(item.number) && !isComplete) ? { onClick: () => moveItem(i, j) } : {} }>{ (item ? item.number : "") }</td>
                    ) }
                    </tr>
                ) }
                </tbody>
            </table>
            { !isComplete ?
                    <p className="badge badge-primary">Количество сделанных ходов: <span className="badge badge-light">{step}</span></p>
                        : <p className="badge badge-pill badge-success">Поздравляем! Вы собрали пазл за {step} ходов</p>
            }
            <button type="button" className="btn btn-info game-container__start-over" onClick={() => startOver()}>Начать сначала</button>
        </div>

GameBoard.propTypes = {
    board: PropTypes.array,
    allowedToMoveItemsNumbers: PropTypes.array,
    step: PropTypes.number,
    isComplete: PropTypes.bool,
    moveItem: PropTypes.func,
    startOver: PropTypes.func
};

export default GameBoard;